from itsdangerous import (TimedJSONWebSignatureSerializer \
                                  as Serializer, BadSignature, \
                                  SignatureExpired)
import time

#---------------------------------------------------------------------------------------
def generate_auth_token(expiration=600):
#---------------------------------------------------------------------------------------
    s = Serializer('test1234@#$', expires_in=expiration)
#---------------------------------------------------------------------------------------
    return s.dumps({'id': 1})

#---------------------------------------------------------------------------------------
def verify_auth_token(token):
    s = Serializer('test1234@#$')
    try:
        data = s.loads(token)
    except SignatureExpired:
        return None    #---------------------------------------------------------------------------------------
    except BadSignature:
        return None    #---------------------------------------------------------------------------------------
    return "Success"


